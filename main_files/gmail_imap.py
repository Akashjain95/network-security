import sys
import imaplib
import getpass
from Tkinter import *
import sys
import imaplib
import tkMessageBox
import getpass
import email
import email.header
import datetime
import tkFileDialog

EMAIL_ACCOUNT = "akash.2016.jain@gmail.com"
EMAIL_FOLDER = "Inbox"
IMAP_SERVER = 'imap.gmail.com'
OUTPUT_DIRECTORY = '/root/Desktop/smtpkeylogs'

def download_mailbox(M):
    """
    Dump all emails in the folder to files in output directory.
    """

    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        #print "No messages found!"
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            tkMessageBox.showinfo("Error", "Unexpected Error!")#print "ERROR getting message", num
            return
        #print "Writing message ", num
        f = open('%s/%s.eml' %(OUTPUT_DIRECTORY, num), 'w')
        f.write(data[0][1])
        f.close()

def download(PASSWORD):

    M = imaplib.IMAP4_SSL(IMAP_SERVER)
    M.login(EMAIL_ACCOUNT, PASSWORD)
    rv, data = M.select(EMAIL_FOLDER)
    if rv == 'OK':
        tkMessageBox.showinfo("Success", "Downloading....")#print "Processing mailbox: ", EMAIL_FOLDER
        download_mailbox(M)
        M.close()
    else:
        tkMessageBox.showinfo("Error", "Unexpected Error!")#print "ERROR: Unable to open mailbox ", rv
    M.logout()

def process_mailbox(M,root):
    """
    Do something with emails messages in the folder.  
    For the sake of this example, print some headers.
    """
    # L1=Label(root,text="Date")
    # L1.place(x=0,y=130)
    L2=Label(root,text="Emails")
    L2.place(x=0,y=130)
    var_y=150
    var_x=0
    rv, data = M.search(None, "ALL")
    if rv != 'OK':
        print "No messages found!"
        return

    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK':
            #print "ERROR getting message", num
            return

        msg = email.message_from_string(data[0][1])
        decode = email.header.decode_header(msg['Subject'])[0]
        subject = unicode(decode[0])
        Label(root,text=subject).place(x=0,y=var_y)
        var_y=var_y+20
        #print 'Message %s: %s' % (num, subject)
        #print 'Raw Date:', msg['Date']
        # Now convert to local date-time
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(
                 email.utils.mktime_tz(date_tuple))
            #print "Local Date:", \
            local_date.strftime("%a, %d %b %Y %H:%M:%S")

def viewlogs(root):
		tkMessageBox.showinfo("Select", "Select log to view!")
		file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file',initialdir=OUTPUT_DIRECTORY)
		if file != None:
			data = str(file.read())
			file.close()
			root2=Tk()
			scrollbar = Scrollbar(root2)
			scrollbar.pack(side=RIGHT, fill=Y)
			text = Text(root2, wrap=WORD, yscrollcommand=scrollbar.set)
			text.insert(1.0,data)
			text.pack()
			scrollbar.config(command=text.yview)
			root2.mainloop()

def login_attempt(root,E1,loginbtn):
    
    pwd=str(E1.get())
    if len(pwd)==0:
       tkMessageBox.showinfo("Invalid", "Enter Valid Password!")
    else:
	
		M = imaplib.IMAP4_SSL('imap.gmail.com')

		try:
			rv, data = M.login(EMAIL_ACCOUNT,pwd)
		except imaplib.IMAP4.error:
			tkMessageBox.showinfo("Invalid", "Enter Valid Password!")
			return
			#print "LOGIN FAILED!!! "
			#sys.exit(1)

#print rv, data

		rv, mailboxes = M.list()
#if rv == 'OK':
#    print "Mailboxes:"
#    print mailboxes

		rv, data = M.select(EMAIL_FOLDER)
		if rv == 'OK':
			tkMessageBox.showinfo("Sucess!", "Processing mailbox...")
			loginbtn.config(state="disabled")
			
			ref_btn=Button(root,text="Refresh",command=lambda:login_attempt(root,E1,loginbtn))
			ref_btn.place(x=50,y=100)
			
			download_btn=Button(root,text="Download all",command=lambda:download(pwd))
			download_btn.place(x=150,y=100)
			
			view_button=Button(root,text="View Logs",command=lambda:viewlogs(root))
			view_button.place(x=250,y=100)
			
			#print "Processing mailbox...\n"
			process_mailbox(M,root)
			M.close()
		else:
			print "ERROR: Unable to open mailbox ", rv

		M.logout()

root=Tk()
root.geometry('{}x{}'.format(400, 400))
#root.iconbitmap(r'C:\Users\Alok Jain\Desktop\favicon.ico')
L1=Label(root,text="Check Your Mail Hacker!")
L1.place(x=0,y=0)

L2=Label(root,text="Enter Your Mail Password!")
L2.place(x=0,y=40)

E1=Entry(root,bd=5,show="*")
E1.place(x=0,y=70)
loginbtn=Button(root,text="Login",command=lambda:login_attempt(root,E1,loginbtn))
loginbtn.place(x=0,y=100)
root.mainloop()
